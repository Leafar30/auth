
import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';
import firebase from 'firebase';
import { Header, Button, CardSection, Spinner } from './Components/Common';
import LoginForm from './Components/LoginForm';



export default class App extends Component {

  state = { loggedIn: null };
   
  componentWillMount() {
    firebase.initializeApp({
        apiKey: 'AIzaSyAo3lcIm_cQAQttipZgalBvdyBMalSjzU8',
        authDomain: 'authentication-b84b2.firebaseapp.com',
        databaseURL: 'https://authentication-b84b2.firebaseio.com',
        projectId: 'authentication-b84b2',
        storageBucket: 'authentication-b84b2.appspot.com',
        messagingSenderId: '267660873328'
    });

    firebase.auth().onAuthStateChanged((user) => {
     if (user) {
       this.setState({ loggedIn: true });
     }else {
       this.setState({ loggedIn: false });
     }
    })
  }

  renderContent() {
   switch (this.state.loggedIn) {
     case true:
     return (<CardSection>
               <Button onPress={() => firebase.auth().signOut()}>Log Out</Button>
            </CardSection>

     ); 
     case false:
     return <LoginForm />;
     default:
     return <Spinner size = "large"/>;
   }
  }
    render() {
      return (
        <View> 
          <Header headerText="Authentication" />
          {this.renderContent()}
        </View>
      );
    }
  }